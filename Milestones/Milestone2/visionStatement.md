﻿						#Mission Statement


						                                  #**Mission Statement**


**For** users that want **to** discuss a news story **where** some facts about the story may be in question. Unlike other social media sites, NewsCheck **is** a social site **where** users can have civil discussions pertaining to any falsehoods found in a specific story provided via a link. To begin using the service, users **must** create an account where posting/replying history **will** be built, their level of trust throughout the community **will** be established, and pages **can** be found by news article, post, or website. The user **who** posts the link **can** write a comment about the incorrect information within the story. Other users of the site will be able **to** post replies **to** the original comment. **Any** comments found **to** be degrading, racial, or insensitive **will** be removed from the site as well as the user. **By** monitoring responses for politeness, we hope **to** create a safe environment **for** people **to** state facts **about** incorrect information **without** worrying about negative responses **from** malicious users. 


					        



					        
