**Description of Team Project**

For years, our service men and women have been plagued with an insufficient way of getting information to nearby VA facilities, a way to discuss numerous subjects, including the reliability of a facility or reaching out to other veterans for information about the facility that they may use, and a resource where they can live chat a friend when they just need someone to speak with to get them through a difficult time in one convenient location. Our team wants to change this and create a better website for veterans to search for the resources they require. The website will be easy to navigate with little clutter in order to easily find what is needed instead of having too many distractions which may create unnecessary obstacles while navigating our site.

What we are striving for is a site where a military service person or family member can signin to a personal account and access VA facility locations, a discussion board, and live chat with other military personnel and families. Eventually, we would like to add features which will include a way to check your benefits, schedule medical visits, and 

**Our Drunken Coders Team:**

* Anthony Visuano

* Chunze Liu

* Anthony Franco

**Guidelines for Contributing Code to the Project**

Please follow the guidelines below when contributing code to this project. We are university students that value our mission to create a resource to support our veterans and appreciate any assistance to build a website that benefits them.

* The programming languages used in this project are C#, JQuery, AJAX, and JSON
* Use XML comments on all methods (any code with missing comments will be rejected)
* Comment comment comment, comment everything, comment some more!
* Use as little short hand as possible, don't be afraid to type out entire names. You might know what WTFSOMANYLETTERS means, but somebody else will be lost in the sauce.

***How to Contribute Code***

Issue a pull request on the develop branch at (add address here). 
\**Only submit code that has been fully tested and functional.*

***Rules to Follow While Writing Code***
* Ensure you're coding what you're supposed to be coding!
* If you need help, research first, then ask!

***What About the Database?***
* Currently just the Azure cheap database

***List of Tools Used***
* Visual Studio Community 2017 version 15.9.6
* Git bash
* More to be added as the project is developed

***Everything to be Installed by NuGet***
* Incoming

**Team Mascot**
![alt-text](mascot.png)


**Team Song**

[![YOUTUBE?](https://img.youtube.com/vi/u_VsvZmIWxY/0.jpg)](https://www.youtube.com/watch?v=u_VsvZmIWxY)

**Software Construction Processes or Lifecycles**

Projected time line:

* Sprint 1 - February 11th - February 24th
* Sprint 2 - February 25th - March 11th
* Sprint 3 - March 12th - March 24th
* End of Winter 2019 term
* **Spring Break - March 25th - March 20th**
* Sprint 4 - April 1st - April 14th
* Sprint 5 - April 15th - April 28th
* Sprint 6 - April 29th - May 12th
* Testing - May 13th - May 26th
* Alpha Release - May 27th - June 7th
* End of Spring 2019 term... did.. did we make it?
