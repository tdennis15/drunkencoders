## Initial Requirements Elaboration and Elicitation
### Questions

1. Will we allow searching for VA facilities anonymously?
    - Yes visitors of all shapes and sizes will be able to search for nearby VA facilities.
2. Could users get directions to VA facilities anonymously?
    - Most likely yes.
3. Will people be allowed to post or read on the discussion board anonymously?
    - Logged in users will be able to post, anyone can read topics on the discussion board.
4. Clearly we need accounts and logins. Our own, or do we allow logging in via 3rd party, i.e. "Log in with Google" or ...?
    - Currently only our own... potential for third party down the road.
5. What is it important to know about our users? What data should we collect?
    - Maybe track previously searched for facilities, if only to quickly access past search results. 
6. How or will the discussion boards be monitored?
    - Moderators will probably be used to keep the peace.
7. Should we put a character limit on discussion posts?
    - Most likely yes, probably around 5000 characters.
8. What kind of discussion do we want to create? Linear traditional, chronological, ranked, or ?
    - Most likely a combo of linear and chronological 
9. Can users post images or links in the discussion board?
    - Currently no, image size could fill our tiny server up too quickly.
10. Will we be storing or hosting those images on our server?
    - Not at this time.
11. How will the live chat be implemented?
    - Still being investigated, but there are several options, from another external API based, to JavaScript based chat.
12. Will the live chat have a set schedule, or allow for requested times?
    - Probably both
13. Will we be using two factor authentication?
    - Currently no, but it could be looked into in the future.
14. Will there be a minimum age requirement to be a registered user or to browse the discussion board?
    - This is a difficult one, currently no. We'd like this to be family friendly.
    
## List of Needs and Features
1. An informative landing page
2. Users can start accounts for custom page
3. A page for our company and philosophy
4. A FAQ page for frequently asked questions
5. Ability to search for a VA facility
6. Maps ability to search for directions to a VA facility
7. Discussion page to ask questions/look for advice
8. Live chat to connect with someone in real time


## Identify Functional Requirements (User Stories)
[E] - Epic, [F] - Feature, [U] - User Stories, [T] - Tasks

1. [U] As a visitor to this site, I'd like a clean and minimal website so that it won't be a struggle to navigate the site to find the information that I need. 
2.  [T] Create starter ASP dot NET MVC 5 Web Application with Individual User Accounts and no unit test project
3.  [T] Switch it over to Bootstrap 4
4.  [T] Create nice homepage: write content, customize navbar
5.  [T] Create SQL Server database on Azure and configure web app to use it. Hide credentials.
6.  [U] As a user, I would like fully enabled Individual User Accounts, so that I will have more options to use the site.
7.  [T] Copy SQL schema from an existing ASP.NET Identity database and integrate it into our UP script
8.  [T] Configure web app to use our db with Identity tables in it
9.  [T] Create a user table and customize user pages to display additional data
10. [E] Implement VA API's to find VA facilities nearby.
11. [U] As a visitor, I want to be able to search for VA facilities nearby, so that I can set up an appointment at the VA facility.
12. [U] As a visitor, I would like to be able to get directions to the VA facility, so that I find the facility without much trouble.
13. [E] Build a discussion board.
    1. [F] Build a well defined table
14. [U] As a logged in user, I want to be able to post to the discussion board, so that I can speak with other veterans about different topics.
15. [U] As a logged in user, I want to be able to live chat with someone, so I can talk through some personal issues without calling the crisis line.
16. [U] As a visitor, I'd like to be able to search the discussion board for specific topics or questions, so I don't have to register if I am only looking for specific information.
17. [U] As a registered user, I would like a complex password system, so that I know my account will be safe from attackers.
18. [U] As a registered user, I would also like a password recovery system, so that I don't have to remember my complex password.
19. [U] As a logged in user, I would like some sort of friend/follow system, so that it's easier for me to keep in touch with other users.
20. [U] As a user/visitor, I would like some sore of filter system, so that bigoted, sexist, or otherwise derogative language won't be filling up every discussion, post, or comment.
21. [U] As a user, I would like some sort of flag system, so that I can alert moderators if any discussion post/comment has hateful, bigoted, sexist, or otherwise derogative language that fell through the cracks.

## Identify Non-Functional Requirements
1. User accounts and data must be stored indefinitely.
2. Site and data must be backed up regularly and have failover redundancy that will allow the site to remain functional in the event of loss of primary web server or primary database. We can live with 1 minute of complete downtime per event and up to 1 hour of read-only functionality before full capacity is restored.
3. Site should never return debug error pages. Web server must never return 404's. All server errors must be logged. Users should receive a custom error page in that case telling them what to do.
4. The site will be in English and Spanish.
5. Security is of up most important, long passwords with requirements of at least one upper case letter, one lower case letter, one number, and one special character to be used.
6. The site should be simple and intuitive to use, i.e. minimal clutter.
7. This site should work on common browsers, such as Firefox, Chrome, Edge (maybe?), Safari, etc..

## Initial Architecture Envisioning
1. ASP.net MVC 5
2. C#
3. SQL Server
4. Azure Web Platform and Services
5. Web Browser
6. Department of Veteran Affairs and Google Maps API's
7. The internet

## Agile Data Modeling
Scrum with Agile Principles

## Time line and release plan
- Sprint 1 - February 11th - 24th, 2019 (Initial release)
- Sprint 2 - February 25th - March 11th 2019 (Second release)
- Sprint 3 - March 12th - 24th, 2019 (Third release)
- Sprint 4 - April 1st - 14th, 2019 (Fourth release)
- Sprint 5 - April 15th - 28th, 2019 (Fifth release)
- Sprint 6 - April 29th - May 12, 2019 (Sixth release)
- Beta Release - May 13 - May 26th, 2019 (Version 1.0 release)
- Alpha Release - May 27th - June 7th, 2019 (Version 1.1 release)
- Done...? To be continued...