﻿using NewsCheck.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NewsCheck.Controllers
{
    public class HomeController : Controller
    {
        private NewsCheckContext db = new NewsCheckContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult UnderConstruction()
        {
            ViewBag.Message = "Page currently under construction!";

            return View();
        }

        [HttpGet]
        public ActionResult NewUserPage()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewUserPage([Bind(Include ="Content, QualityLevel, UsersID")] Comment comment)
        {
            if(ModelState.IsValid)
            {
                db.Comments.Add(comment);
                db.SaveChanges();
                return Redirect("NewUserPage");
            }
            return View("Home");
        }
    }
}