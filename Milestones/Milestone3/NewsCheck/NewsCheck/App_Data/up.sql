﻿CREATE TABLE [dbo].[Users]
(
  UsersID INT IDENTITY(1,1) NOT NULL,
  Name NVARCHAR(30) NOT NULL,
  EmailAddress NVARCHAR(50) NOT NULL,
  CommentHistory INT,
  Trustlevel INT,
  CreationDate DATETIME NOT NULL,
  Password NVARCHAR(50) NOT NULL,
  CONSTRAINT [PK_dbo.Users] PRIMARY KEY CLUSTERED (UsersID ASC)
);

CREATE TABLE [dbo].[Moderator]
(
  ModeratorID INT IDENTITY(1,1) NOT NULL,
  UsersID INT NOT NULL,
  CONSTRAINT [PK_dbo.Moderator] PRIMARY KEY CLUSTERED (ModeratorID ASC),
  CONSTRAINT [FK_dbo.Users] FOREIGN KEY (UsersID) REFERENCES Users(UsersID)
  ON DELETE CASCADE
		ON UPDATE CASCADE
);

CREATE TABLE [dbo].[Comments]
(
  CommentsID INT IDENTITY(1,1) NOT NULL,
  Content NVARCHAR(MAX) NOT NULL,
  QualityLevel INT NOT NULL,
  TimeStamp DATETIME NOT NULL,
  UsersID INT NOT NULL,
  CONSTRAINT [PK_dbo.Comments] PRIMARY KEY CLUSTERED (CommentsID ASC),
  CONSTRAINT [FK2_dbo.Users] FOREIGN KEY (UsersID) REFERENCES Users(UsersID)
);

CREATE TABLE [dbo].[Discussions]
(
  DiscussionsID INT IDENTITY(1,1) NOT NULL,
  SourceLink NVARCHAR(MAX) NOT NULL,
  CommentsID INT NOT NULL,
  CONSTRAINT [PK_dbo.Discussions] PRIMARY KEY CLUSTERED (DiscussionsID ASC),
  CONSTRAINT [FK_dbo.Comments] FOREIGN KEY (CommentsID) REFERENCES Comments(CommentsID)
);

CREATE TABLE [dbo].[UserRating]
(
  UserRatingID INT IDENTITY(1,1) NOT NULL,
  UsersID INT NOT NULL,
  UpJustification NVARCHAR(MAX) NOT NULL,
  DownJustification NVARCHAR(MAX) NOT NULL,
  FlagJustification NVARCHAR(MAX) NOT NULL,
  CONSTRAINT [PK_dbo.UserRating] PRIMARY KEY CLUSTERED (UserRatingID ASC),
  CONSTRAINT [FK3_dbo.Users] FOREIGN KEY (UsersID) REFERENCES Users(UsersID)
);

CREATE TABLE [dbo].[CommentRating]
(
  CommentRatingID INT IDENTITY(1,1) NOT NULL,
  CommentsID INT NOT NULL,
  UpJustification NVARCHAR(MAX) NOT NULL,
  DownJustification NVARCHAR(MAX) NOT NULL,
  FlagJustification NVARCHAR(MAX) NOT NULL,
  CONSTRAINT [PK_dbo.CommentRating] PRIMARY KEY CLUSTERED (CommentRatingID ASC),
  CONSTRAINT [FK2_dbo.Comments] FOREIGN KEY (CommentsID) REFERENCES Comments(CommentsID)
);

INSERT INTO dbo.Users(Name,EmailAddress,CommentHistory,Trustlevel,CreationDate,Password)VALUES
('Chunze L','15042295553@163.com',5,10,'2019-1-27','123456'),
('Anthony F','supermail@some.com',5,10,'2019-1-27','15555'),
('Anthony V','legitemail@place.com',5,10,'2019-1-27','159632');



