namespace WhiskeyTangoFoxtrot.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Forum")]
    public partial class Forum
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Forum()
        {
            ForumPosts = new HashSet<ForumPost>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ForumID { get; set; }

        public int WTFUserID { get; set; }

        [Required]
        [StringLength(64)]
        public string ForumName { get; set; }

        [Required]
        [StringLength(256)]
        public string Message { get; set; }

        public bool? IsActive { get; set; }

        public bool? IsMemberOnly { get; set; }

        public virtual WTFUser WTFUser { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ForumPost> ForumPosts { get; set; }
    }
}
