namespace WhiskeyTangoFoxtrot.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ForumPost")]
    public partial class ForumPost
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ForumPostID { get; set; }

        public int WTFUserID { get; set; }

        public int ForumID { get; set; }

        [Required]
        [StringLength(1024)]
        public string PostMessage { get; set; }

        public int? ReplyID { get; set; }

        public virtual Forum Forum { get; set; }

        public virtual WTFUser WTFUser { get; set; }
    }
}
