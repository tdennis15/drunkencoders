namespace WhiskeyTangoFoxtrot.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class WTFContext : DbContext
    {
        public WTFContext()
            : base("name=WTFContext")
        {
        }

        public virtual DbSet<AspNetRole> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
        public virtual DbSet<ChatBox> ChatBoxes { get; set; }
        public virtual DbSet<ChatMessage> ChatMessages { get; set; }
        public virtual DbSet<Forum> Fora { get; set; }
        public virtual DbSet<ForumPost> ForumPosts { get; set; }
        public virtual DbSet<UserProfile> UserProfiles { get; set; }
        public virtual DbSet<WTFUser> WTFUsers { get; set; }
        public virtual DbSet<City> Cities { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AspNetRole>()
                .HasMany(e => e.AspNetUsers)
                .WithMany(e => e.AspNetRoles)
                .Map(m => m.ToTable("AspNetUserRoles").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.AspNetUserClaims)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.AspNetUserLogins)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<ChatBox>()
                .HasMany(e => e.ChatMessages)
                .WithRequired(e => e.ChatBox)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<WTFUser>()
                .HasMany(e => e.ChatMessages)
                .WithRequired(e => e.WTFUser)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<WTFUser>()
                .HasMany(e => e.ForumPosts)
                .WithRequired(e => e.WTFUser)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<WTFUser>()
                .HasOptional(e => e.UserProfile)
                .WithRequired(e => e.WTFUser)
                .WillCascadeOnDelete();

            modelBuilder.Entity<City>()
                .Property(e => e.Latitude)
                .HasPrecision(8, 6);

            modelBuilder.Entity<City>()
                .Property(e => e.Longitude)
                .HasPrecision(9, 6);
        }
    }
}
