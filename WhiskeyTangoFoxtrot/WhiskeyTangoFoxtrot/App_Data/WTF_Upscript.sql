﻿CREATE TABLE [dbo].[WTFUsers] (
	
	[WTFUserID]			INT IDENTITY (1,1)	NOT NULL,
	[Username]			NVARCHAR(64)		NULL,
	[LoginCount]		INT					DEFAULT 1,
	[AspNetIdentityID]	NVARCHAR(128)		NOT NULL,

	CONSTRAINT [PK_dbo.WTFUsers] PRIMARY KEY CLUSTERED ([WTFUserID] ASC)
);

CREATE TABLE [dbo].[UserProfile] (
	[WTFUserID]					INT					NOT NULL,
	[FirstName]					NVARCHAR(64)		NOT NULL,
	[LastName]					NVARCHAR(64)		NOT NULL,
	[UserCity]					NVARCHAR(64)		NULL,
	[Bio]						NVARCHAR (max)		NULL,
	[ProfileIsPrivate]			Bit					NOT NULL,

	CONSTRAINT [PK_dbo.UserProfile.WTFUserID] PRIMARY KEY CLUSTERED	([WTFUserID] ASC),

	CONSTRAINT [FK_dbo.UserProfile.WTFUserID] 
	FOREIGN KEY ([WTFUserID]) REFERENCES [dbo].[WTFUsers] ([WTFUserID]) 
	ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE [dbo].[Forum] (
	[ForumID]		INT				NOT NULL,
	[WTFUserID]		INT				NOT NULL,
	[ForumName]		NVARCHAR(64)	NOT NULL,
	[Message]		NVARCHAR(256)	NOT NULL,
	[IsActive]		Bit				NULL,
	[IsMemberOnly]	Bit				NULL,

	CONSTRAINT [PK_dbo.Forum.ForumID] PRIMARY KEY CLUSTERED ([ForumID] ASC),

	CONSTRAINT [FK_dbo.Forum.WTFUserID]
	FOREIGN KEY ([WTFUserID]) REFERENCES [dbo].[WTFUsers] ([WTFUserID])
	ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE [dbo].[ForumPost] (
	[ForumPostID]	INT				NOT NULL,
	[WTFUserID]		INT				NOT NULL,
	[ForumID]		INT				NOT NULL,
	[PostMessage]	NVARCHAR(1024)	NOT NULL,
	[ReplyID]		INT				NULL,

	CONSTRAINT [PK_dbo.Forum.ForumPostID] PRIMARY KEY CLUSTERED ([ForumPostID] ASC),

	CONSTRAINT [FK_dbo.ForumPost.WTFUserID]
	FOREIGN KEY ([WTFUserID]) REFERENCES [dbo].[WTFUsers] ([WTFUserID]),

	CONSTRAINT [FK_dbo.ForumPost.ForumID]
	FOREIGN KEY ([ForumID]) REFERENCES [dbo].[Forum] ([ForumID])
	ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE [dbo].[ChatBox] (
	[ChatBoxID]		INT				NOT NULL,
	[ChatName]		NVARCHAR(64)	NOT NULL,
	[ChatTopic]		NVARCHAR(64)	NOT NULL,
	[Message]		NVARCHAR(256)	NULL,
	[IsMemberOnly]	Bit				NULL,

	CONSTRAINT [PK_dbo.ChatBox.ChatBoxID] PRIMARY KEY CLUSTERED ([ChatBoxID] ASC),
);

CREATE TABLE [dbo].[ChatMessage] (
	[MessageID]			INT			NOT NULL,	
	[WTFUserID]			INT			NOT NULL,
	[ChatBoxID]			INT			NOT NULL,
	[MessageDate]		DateTime	NOT NULL,
	[MessageContent]	NVARCHAR	NOT NULL,
	[ReplyID]			INT			NULL

	CONSTRAINT [PK_dbo.ChatMessage.MessageID] PRIMARY KEY CLUSTERED ([MessageID] ASC),

	CONSTRAINT [FK_dbo.ChatMessage.WTFUserID] 
	FOREIGN KEY ([WTFUserID]) REFERENCES [dbo].[WTFUsers] ([WTFUserID]),

	CONSTRAINT [FK_dbo.ChatMessage.ChatBoxID] 
	FOREIGN KEY ([ChatBoxID]) REFERENCES [dbo].[ChatBox] ([ChatBoxID]),
);

CREATE TABLE [dbo].[Cities](
	[CityID]		INT	IDENTITY(1,1)	NOT NULL,
	[City]			NVARCHAR(32)		NOT NULL,
	[Latitude]		DECIMAL(8,6)		NOT NULL,
	[Longitude]		DECIMAL(9,6)		NOT NULL,

	CONSTRAINT [PK_dbo.CityID] PRIMARY KEY CLUSTERED ([CityID] ASC) 
);

CREATE TABLE [dbo].[SavedCities](
	[WTFUserID]		INT		NOT NULL,
	[CityID]		INT		NOT NULL
	UNIQUE([WTFUserID],[CityID]),

	CONSTRAINT [FK_dbo.WTFUserID]
	FOREIGN KEY ([WTFUserID]) REFERENCES [dbo].[WTFUsers] ([WTFUserID]),

	CONSTRAINT [FK_dbo.CityID]
	FOREIGN KEY ([CityID]) REFERENCES [dbo].[Cities] ([CityID])
);