﻿using System.Diagnostics;
using System.IO;
using System.Net;
using WhiskeyTangoFoxtrot.Models;
using System.Web.Mvc;
using System.Linq;

namespace WhiskeyTangoFoxtrot.Controllers
{
    public class VAFacilitySearchController : Controller
    {
        //readonly string key = System.Web.Configuration.WebConfigurationManager.AppSettings["apiKey"];
        readonly string key = "0YUKwjtLZXSkS9gYZ8J8uXvI2oaCplrs";

        private WTFContext db = new WTFContext();

        // Search for VA Facilties view page
        public ActionResult Search()
        {
            return View(db.Cities.ToList());
        }
        
        /// <summary>
        /// Main work horse of the controller, constructs a bounding box, embeds api key in the header of the html request,
        /// and returns a json result of all the data for facilities in the area of the bounding box.
        /// </summary>
        /// <param name="lat">User submitted latitude</param>
        /// <param name="lon">User submitted longitude</param>
        /// <returns>JSON array with VA facility information</returns>
        [HttpGet]
        public JsonResult FacilitySearch(decimal? lat, decimal? lon)
        {
            // Send latitude and longitude to the helper method to construct the array
            decimal?[] box = FacilitySearchHelper(lat, lon);

            // example query string formats, by BBOX and by ID using BBOX for application currently
            //Error Code 400 means inproper query string
            //<https://dev-api.va.gov/services/va_facilities/v0/facilities?bbox%5B%5D=-120&bbox%5B%5D=40&bbox%5B%5D=-125&bbox%5B%5D=50>
            //string qurl = "https://dev-api.va.gov/services/va_facilities/v0/facilities/?" + "ids=vha_668";

            // String constructed from the bounding box
            string qurl = "https://dev-api.va.gov/services/va_facilities/v0/facilities/?bbox%5B%5D=" 
                + box[0] + "&bbox%5B%5D=" + box[1] + "&bbox%5B%5D=" + box[2] + "&bbox%5B%5D=" + box[3];

            // Build the api request with added key in header
            WebRequest request = WebRequest.Create(qurl);
            request.Headers["apiKey"] = key;

            // Open the response and get the data
            WebResponse response = request.GetResponse();
            Stream stream = response.GetResponseStream();
            string reader = new StreamReader(stream).ReadToEnd();

            // Make it usable 
            var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var data = serializer.DeserializeObject(reader);

            // Clean up
            stream.Close();
            response.Close();

            // Return with the data, list of VA facilities
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        // Build the array used for the bounding box based on lat/long provided
        public decimal?[] FacilitySearchHelper(decimal? lat, decimal? lon)
        {
            // Grab the two longitude parameters for the box
            decimal? a1 = lon + (decimal)0.5;
            decimal? a2 = lon - (decimal)0.5;

            // Grab the two latitude parameters for the box
            decimal? b1 = lat + (decimal)0.5;
            decimal? b2 = lat - (decimal)0.5;

            // Fill the box with an order of a1,b1,a2,b2 (long/lat, long/lat)
            decimal?[] box = new decimal?[4];
            box[0] = a1;
            box[1] = b1;
            box[2] = a2;
            box[3] = b2;

            // Return the bounding box coordinates
            return box;
        }
    }
}