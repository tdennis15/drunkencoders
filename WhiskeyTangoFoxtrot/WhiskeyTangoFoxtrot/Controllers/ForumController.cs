﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using WhiskeyTangoFoxtrot.Models;

namespace WhiskeyTangoFoxtrot.Controllers
{
    public class ForumController : Controller
    {
        private WTFContext db = new WTFContext();

        // GET: Forum
        public ActionResult Index()
        {
            var fora = db.Fora.Include(f => f.WTFUser);
            return View(fora.ToList());
        }

        // GET: Forum/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Forum forum = db.Fora.Find(id);
            if (forum == null)
            {
                return HttpNotFound();
            }
            return View(forum);
        }

        // GET: Forum/Create
        [Authorize]
        public ActionResult Create()
        {
            ViewBag.WTFUserID = new SelectList(db.WTFUsers, "WTFUserID", "Username");
            return View();
        }

        // POST: Forum/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ForumName,Message,IsActive,IsMemberOnly")] Forum forum)
        {
            forum.ForumID = db.Fora.Count() + 1;
            forum.WTFUserID = GetUserID();
            if (ModelState.IsValid)
            {
                db.Fora.Add(forum);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.WTFUserID = new SelectList(db.WTFUsers, "WTFUserID", "Username", forum.WTFUserID);
            return View(forum);
        }

        // GET: Forum/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Forum forum = db.Fora.Find(id);
            if (forum == null)
            {
                return HttpNotFound();
            }

            var user = GetUserID();
            if (forum.WTFUserID != user)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ViewBag.WTFUserID = new SelectList(db.WTFUsers, "WTFUserID", "Username", forum.WTFUserID);
            return View(forum);
        }

        // POST: Forum/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ForumID,WTFUserID,ForumName,Message,IsActive,IsMemberOnly")] Forum forum)
        {
            if (ModelState.IsValid)
            {
                db.Entry(forum).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.WTFUserID = new SelectList(db.WTFUsers, "WTFUserID", "Username", forum.WTFUserID);
            return View(forum);
        }

        // GET: Forum/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Forum forum = db.Fora.Find(id);
            if (forum == null)
            {
                return HttpNotFound();
            }
            return View(forum);
        }

        // POST: Forum/Delete/5
        [HttpPost, ActionName("Delete")]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Forum forum = db.Fora.Find(id);
            db.Fora.Remove(forum);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private int GetUserID()
        {
            string user1 = User.Identity.GetUserId();

            WTFUser user = db.WTFUsers.Where(a => a.AspNetIdentityID == user1).FirstOrDefault();

            int UserID = user.WTFUserID;

            return UserID;
        }
    }
}
