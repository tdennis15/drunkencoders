﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using WhiskeyTangoFoxtrot.Models;
using WhiskeyTangoFoxtrot.Models.Viewmodels;

namespace WhiskeyTangoFoxtrot.Controllers
{
    public class ProfileController : Controller
    {
        WTFContext dbContext = new WTFContext();
       
        // GET: Profile
        [Authorize]
        public ActionResult UserIndex()
        {
            ProfileViewModel profileViewModel = CreateProfileViewModel();
            return View(profileViewModel);
        }


        [Authorize]
        private ProfileViewModel CreateProfileViewModel()
        {

            int UserID = GetUserID();

            ProfileViewModel profileViewModel = new ProfileViewModel
            {
                WTFUsers = dbContext.WTFUsers.Where(a => a.WTFUserID == UserID),

                UserProfiles = dbContext.WTFUsers.Where(a => a.WTFUserID == UserID).Select(b => b.UserProfile),

                Forums = dbContext.Fora.Where(a => a.WTFUserID == UserID).ToList()
            };

            return profileViewModel;
        }

        public int IncrementInput(int input)
        {
          return input + 1;
        }

        private int GetUserID()
        {
            string user1 = User.Identity.GetUserId();

            WTFUser user = dbContext.WTFUsers.Where(a => a.AspNetIdentityID == user1).FirstOrDefault();

            int UserID = user.WTFUserID;

            return UserID;
        }
    }
}