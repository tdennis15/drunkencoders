﻿$(document).ready(function () {
});

//Code refactoring provided by TJ Dennis 4/13/2019 to implement API functionality
function findCity(lat, lng) {
    
    // Build the search query from user input
    var src = "FacilitySearch?lat=" + lat + "&lon=" + lng;
    //console.log(src);

    $.ajax({
        url: src,
        type: 'GET',
        dataType: 'json',
        success: returnFacilities,
        error: failed
    });
}

// Return the JSON data to the page
function returnFacilities(data) {

    // Assign the data to tmp (data.data was getting tiresome)
    var tmp = data.data;
    //console.log("Tmp variable name at index 0: " + tmp[0].attributes.name);

    // Clear the previous search results
    $('#results').empty();

    // Table was too cluttered and hard to read, this is much more readable
    for (var i = 0; tmp.length; i++) {

        // If the website is NOT empty(null) or set to 'NULL' append Name/Address/Phone/Hours/Website else just append Name/Address/Phone/Hours
        if (tmp[i].attributes.website !== null && tmp[i].attributes.website !== 'NULL') {
            $('#results').append('<div style="padding:2px; text-align: center;"><br>'
                + '<h5><b> ' + tmp[i].attributes.name + ' </b></h5>'
                + '<p>'
                + tmp[i].attributes.address.physical.address_1 + ', ' + tmp[i].attributes.address.physical.city + ' '
                + tmp[i].attributes.address.physical.state + ', ' + tmp[i].attributes.address.physical.zip + '<br>'
                + tmp[i].attributes.phone.main + '<br>'
                + 'Hours: ' + tmp[i].attributes.hours.tuesday + '<br>'
                + tmp[i].attributes.website + '<br>'
                + '</p>' + '</div>'
            );
        } else {
            $('#results').append('<div style="padding:2px; text-align: center;"><br>'
                + '<h5><b> ' + tmp[i].attributes.name + ' </b></h5>'
                + '<p>'
                + tmp[i].attributes.address.physical.address_1 + ', ' + tmp[i].attributes.address.physical.city + ' '
                + tmp[i].attributes.address.physical.state + ', ' + tmp[i].attributes.address.physical.zip + '<br>'
                + tmp[i].attributes.phone.main + '<br>'
                + 'Hours: ' + tmp[i].attributes.hours.tuesday + '<br>'
                + '</p>' + '</div>'
            );
        }
    }
};

// Hopefully we don't hit this
function failed() {
    alert(['You fell down']);
}