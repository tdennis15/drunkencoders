﻿using System;
using WhiskeyTangoFoxtrot.Controllers;
using System.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WTFTests
{
    [TestClass]
    public class TJUnitTests
    {
        [TestMethod]
        public void IsInputIncremented_ReturnsTrue()
        {
            //Define 
            int a = 1;

            //Create
            ProfileController prof = new ProfileController();

            //Assert
            var result = prof.IncrementInput(a);
            Assert.AreEqual(a + 1, result);
        }

        [TestMethod]
        public void IsInputIncremented_ReturnsFalse()
        {
            //Define 
            int a = 1;

            //Create
            ProfileController prof = new ProfileController();

            //Assert
            var result = prof.IncrementInput(a);
            Assert.AreNotEqual(a, result);
        }
    }
}

