﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WhiskeyTangoFoxtrot.Controllers;

namespace WTFTests
{
    [TestClass]
    public class AVUnitTest
    {
        [TestMethod]
        public void BoundingBoxHelperMethodReturnsTrue()
        {
            // Define
            decimal a = 1;
            decimal b = 2;

            // Create 
            VAFacilitySearchController vafs = new VAFacilitySearchController();

            // Assert
            var result = vafs.FacilitySearchHelper(a, b);
            Assert.AreEqual(a + (decimal)1.5, result[0]);
            Assert.AreEqual(a + (decimal)0.5, result[1]);
            Assert.AreEqual(b - (decimal)0.5, result[2]);
            Assert.AreEqual(b - (decimal)1.5, result[3]);
        }

        [TestMethod]
        public void BoundingBoxHelperMethodReturnsFalse()
        {
            // Define
            decimal a = 1;
            decimal b = 2;

            // Create 
            VAFacilitySearchController vafs = new VAFacilitySearchController();

            // Assert
            var result = vafs.FacilitySearchHelper(a, b);
            Assert.AreNotEqual(a - 2, result[0]);
            Assert.AreNotEqual(a - 1, result[1]);
            Assert.AreNotEqual(b + 1, result[2]);
            Assert.AreNotEqual(b + 2, result[3]);
        }
    }
}
